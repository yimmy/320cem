<!DOCTYPE html>
<html lang="en">
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your own takeaway menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/heroic-features.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">TakeAway Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="inputMenu.php">Add Restaurant
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="search.php">Find Restaurant
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<!-- search options -->
<div class="container">
    <?php
    if (empty($_GET["restId"])) {
        echo "<h1>Error: Access with no restaurant data</h1>";
        echo "<h1>Please go back to the search page!";
    } else {
        require_once('connection/conn.php');
        if ($conn->connect_error) {
            die("CONNECTION FAILED! " . $conn->connect_error);
        } else {
            mysqli_query($conn, "set character set 'utf8'");//读库 解決中文亂碼問題
            mysqli_query($conn, "set names 'utf8'");//写库 解決中文亂碼問題
            //query restaurant information by $id
            $id = $_GET["restId"];
            $id = preg_replace('/[^\p{L}\p{N}\s]/u', "", $id);
            $sql = "SELECT * FROM restaurantdetail WHERE ID='" . $id . "'";
            $result = $conn->query($sql);

            //query menu image location from database
            $sql = "SELECT * FROM resourseimg WHERE restaurantID='" . $id . "'";
            $imgResult = $conn->query($sql);

            //print out restaurant information
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<h1 class=\"display-3\" align=\"center\">" . $row["name"] . "</h1>";

                    echo "<hr />";

                    echo "<table class=\"table table-striped table-dark\">";
                    echo "<tr>";
                    echo "<th scope=\"col\">Restaurant Name: </th>";
                    echo "<td>" . $row["name"] . "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th scope=\"col\">Type: </th>";
                    echo "<td>" . $row["type"] . "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th scope=\"col\">Opening Hours: </th>";
                    echo "<td>" . date("h:ia", strtotime($row["open"])) . " to " . date("h:ia", strtotime($row["close"])) . "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th scope=\"col\">Average Price: </th>";
                    echo "<td>$" . $row["price"] . "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th scope=\"col\">Address: </th>";
                    echo "<td>" . $row["location"];
                    echo "<br />";
                    if ($row["LocationX"] == 0 && $row["LocationY"] == 0) {
                        echo "the address for restaurant is invalid, please correct!";
                    } else {
                        $lat = $row["LocationX"];
                        $lng = $row["LocationY"];
                        echo "<iframe src=\"http://maps.google.com/maps?q=" . $lat . "," . $lng . "&z=15&output=embed\"></iframe>";
                    }
                    echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th scope=\"col\">Phone: </th>";
                    echo "<td>" . $row["phone"] . "</td>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th scope=\"col\">Menu(s): </th>";
                    if ($imgResult->num_rows > 0) {
                        echo "<td>";
                        while ($imgRow = $imgResult->fetch_assoc()) {
                            echo "<img src=\"" . $imgRow["image"] . "\" style=\"width:100%; \">";
                            echo "<br /><br />";
                        }
                        echo "</td>";
                    } else {
                        echo "<td>no menu yet!</td>";
                    }
                    echo "</tr>";
                    echo "<tr>";
                    $updateUrl = "update.php?restId=" . $id;
                    echo "<td align=\"center\" colspan=\"2\">
                    Want to update the restaurant information? <br /><br />
                    <a href=\"" . $updateUrl . "\"><button class=\"btn btn-primary btn-lg\">Edit Now!</button></a>
                    </td>";
                    echo "<tr />";
                    echo "</table>";
                }
            } else {
                echo "<h1 class=\"display-3\" align=\"center\">Restaurant id invalid! No records found!</h1>";
            }
        }
        $conn->close();
    }
    ?>
</div>

</body>

</html>
