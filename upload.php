<?php
require_once 'connection/conn.php';
$Name = $_POST['rName'];
$Location = $_POST['rAddress'];
$lat = $_POST['lat'];
$lng = $_POST['lng'];
$Type = $_POST['rType'];
$Phone = $_POST['rPhone'];
$Open = $_POST['roTime'];
$Close = $_POST['rcTime'];
$Price = $_POST['rPrice'];
$last_id = 0;
$target_dir = "uploads/";
if (!file_exists('uploads/')) {//檢查文件夾是否存在
    mkdir("uploads/");    //沒有就創建一個新文件夾
}


$uploadOk = 1;


$arr = array(); //定义一个数组存放上传图片的名称方便你以后会用的。
$count = 0;


// Check if image file is a actual image or fake image
if (isset($_POST["save"])) {
    if ($lat == null) {
        $lat = 0;
        $lng = 0;
    }
    $sql = "INSERT INTO restaurantdetail (name, location,LocationX,LocationY, type , phone ,open ,close,  price)
VALUES ('" . $Name . "', '" . $Location . "', '" . $lat . "', '" . $lng . "', '" . $Type . "','" . $Phone . "','" . $Open . "','" . $Close . "','" . $Price . "')";
    mysqli_query($conn, "set character set 'utf8'");//读库
    mysqli_query($conn, "set names 'utf8'");//写库
    if (mysqli_query($conn, $sql)) {

        global $last_id;
        $last_id = mysqli_insert_id($conn);
        echo "New record created successfully. Last inserted ID is: " . $last_id;
    } else {
        echo "<br/>Fail" . '<br/>';
    }


    foreach ($_FILES["rImg"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {


            $tmp_name = $_FILES["rImg"]["tmp_name"][$key];
            $a = explode(".", $_FILES["rImg"]["name"][$key]); //截取文件名跟后缀
            // $prename = substr($a[0],10); //如果你到底的图片名称不是你所要的你可以用截取字符得到
            $prename = $a[0];
            $name = date('YmdHis') . mt_rand(100, 999) . "." . $a[1]; // 文件的重命名 （日期+随机数+后缀）
            $uploadfile = $target_dir . $name; // 文件的路径
            $imageFileType = strtolower(pathinfo($uploadfile, PATHINFO_EXTENSION));

            $check = getimagesize($_FILES["rImg"]["tmp_name"][$key]);

            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;

            }
            if (file_exists($uploadfile)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }

            // Check file size
            if ($_FILES["rImg"]["size"][$key] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($tmp_name, $uploadfile)) {
                    echo "The file " . $uploadfile . " has been uploaded.";//$uploadfile是圖片的路徑
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            $arr[$count] = $uploadfile;

            global $last_id;
            $sql2 = "INSERT INTO resourseimg (restaurantID,image) VALUES ('" . $last_id . "','" . $uploadfile . "')";
            mysqli_query($conn, $sql2);


            $count++;
        }
    }
}
echo "You will be redirected to the corresponding restaurant home page in few seconds.";
header("refresh:3;index.php");


//header("location:inputMenu.php");
?>