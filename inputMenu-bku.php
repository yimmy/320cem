<!DOCTYPE html>
<html lang="en">
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
 
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Your own takeaway menu</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/heroic-features.css" rel="stylesheet">

  </head>

  <body>
<?php 
$target_dir = "uploads/";
if (!file_exists('uploads/')) {//檢查文件夾是否存在
    mkdir("uploads/");    //沒有就創建一個新文件夾
}
$target_file = $target_dir . basename($_FILES["upload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if (isset($_POST["save"])) {
    $check = getimagesize($_FILES["upload"]["tmp_name"]);
    if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["upload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {
        echo "The file " . $target_file . " has been uploaded.";//$target_file是圖片的路徑
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

?>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">TakeAway Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

    <form class="form-horizontal"  action="upload.php" method="post">
    <fieldset>
		<legend>Please input detail</legend>
        <div class="form-group">
          <label class="col-md-4 control-label" for="name">Restaurant Name</label>  
          <div class="col-md-4">
          <input id="rName" name="rName" type="text" placeholder="Restaurant Name" class="form-control input-md" required="">
          </div>
        </div>
		
		<div class="form-group">
          <label class="col-md-4 control-label" for="name">Type of restaurant</label>  
          <div class="col-md-4">
          <input id="rType" name="rType" type="text" placeholder="Type of restaurant" class="form-control input-md" required="">
          </div>
        </div>
		
        <div class="form-group">
          <label class="col-md-4 control-label" for="address">Address</label>  
          <div class="col-md-7">
          <input id="rAddress" name="rAddress" type="text" placeholder="Restaurant address" class="form-control input-md" required=""> 
          </div>
        </div>
	    <script>

      var placeSearch, autocomplete;
	  var temLat,temLng;
	  var input = document.getElementById('rAddress');
		function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(input);

       
        autocomplete.addListener('place_changed', fillInAddress);
      }
	     function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
		temLat = place.geometry.location.lat();
		temLng = place.geometry.location.lng();
		document.getElementById('lat').value = temLat;
		document.getElementById('lng').value = temLng;		
      }
    </script>
        <div class="form-group">
          <label class="col-md-4 control-label" for="address1">Open and Close time</label>  
          <div class="col-md-4">
          
		  <input id="oTime" name="roTime" type="time" placeholder="Open time" class="form-control input-md" required="">
		  <span class="help-block"><center>to</center></span>
		  <input id="cTime" name="rcTime" type="time" placeholder="Close time" class="form-control input-md" required="">
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-4 control-label" for="phone">Phone Number</label>  
          <div class="col-md-4">
          <input id="rPhone" name="rPhone" type="text" placeholder="Restaurant Phone Number" class="form-control input-md" required="">  
          </div>
        </div>

       <div class="form-group">
          <label class="col-md-4 control-label" for="price">Average price</label>  
          <div class="col-md-4">
          <input id="rPrice" name="rPrice" type="number" placeholder="Restaurant Average price" class="form-control input-md" required="">  
          </div>
        </div>
		
        <div class="form-group">
          <label class="col-md-4 control-label" for="img">Upload the menu</label>  
          <div class="col-md-4">
          <input type="file" id="rImg" name="rImg" placeholder="Upload the image file" class="form-control input-md" required="">
    
          </div>
        </div>
 <input type="hidden" id="lat" name="lat" >
  <input type="hidden" id="lng" name="lng" >

        <div class="form-group">
          <label class="col-md-4 control-label" for="save"></label>
          <div class="col-md-8">
            <button type="submit" id="save" name="save" class="btn btn-success">Add</button>
            <button id="cancel" name="cancel" class="btn btn-danger">Cancel</button>
          </div>
        </div>

        </fieldset>
        </form>
</div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_jtoEQdm4NtyokURH_LO-_f2wUYJTEKs&libraries=places&callback=initAutocomplete&language=en"></script>
  </body>

</html>
